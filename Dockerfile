FROM debian:9 as less

RUN apt update && apt install -y wget gcc make libpcre3 libpcre3-dev zlib1g zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.20.2.tar.gz && tar xvfz nginx-1.20.2.tar.gz && cd nginx-1.20.2 && ./configure && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin/
COPY --from=less /usr/local/nginx/sbin/nginx .
RUN mkdir ../logs ../html ../conf && touch ../logs/error.log && chmod +x nginx
COPY --from=less /usr/local/nginx/conf /usr/local/nginx/conf
COPY --from=less /usr/local/nginx/html /usr/local/nginx/html
EXPOSE 80
CMD [ "/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]

