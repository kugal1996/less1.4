sudo docker run -v /home/ftpuser/do/nginx.conf:/usr/local/nginx/conf/nginx.conf -d -p 8005:80 less4:v1
41f0fbe839721b437f6aadfc79c3be658c7f011175fa3f6f58d45e321e742522
CONTAINER ID   IMAGE      COMMAND                  CREATED          STATUS          PORTS                                   NAMES
41f0fbe83972   less4:v1   "/usr/local/nginx/sb…"   32 seconds ago   Up 31 seconds   0.0.0.0:8005->80/tcp, :::8005->80/tcp   cool_wright
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
